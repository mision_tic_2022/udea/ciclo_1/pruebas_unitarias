import unittest
import operaciones

class TestOperaciones(unittest.TestCase):
    
    def test_sumar(self):
        self.assertEqual( operaciones.sumar(5,10), 15 )
        self.assertNotEqual( operaciones.sumar(10,10), 0 )
    
    def test_restar(self):
        self.assertEqual( operaciones.restar(10,5), 5 )
        self.assertEqual( operaciones.restar(5,10), -5 )
    
    def test_sumar_elementos(self):
        self.assertEqual( operaciones.sumar_elementos([1,2,3,4,5]), 15 )
        self.assertNotEqual( operaciones.sumar_elementos([10,20,30,40]), 120 )
        self.assertRaises(TypeError, operaciones.sumar_elementos, [10,20,'hola', 'mundo'])
    
    def test_elevar_al_cuadrado(self):
        self.assertEqual( operaciones.elevar_al_cuadrado([2,3,4]), [4,9,16] )
        self.assertEqual(operaciones.elevar_al_cuadrado([10,20,'hola', 'mundo']), 0)
    

if __name__ == '__main__':
    unittest.main()