
from functools import reduce


sumar = lambda n1,n2: n1+n2

restar = lambda n1,n2: n1-n2

def sumar_elementos(lista: list):
    try:
        suma = reduce( lambda ac,e: ac+e, lista )
        return suma
    except:
        return 0

def elevar_al_cuadrado(numeros: list):
    try:
        numeros = list( map(lambda n: n**2, numeros) )
        return numeros
    except:
        return 0